package com.vr.miniautorizador.service.impl;

import com.vr.miniautorizador.entity.Card;
import com.vr.miniautorizador.entity.dtos.CardDto;
import com.vr.miniautorizador.entity.dtos.CardTransactionDto;
import com.vr.miniautorizador.repository.AutorizadorRepository;
import com.vr.miniautorizador.util.Mapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class AutorizadorServiceImplTest {

    @Mock
    private AutorizadorRepository repository;

    @Mock
    private Mapper mapper;

    @InjectMocks
    private AutorizadorServiceImpl autorizadorService;

    @Test
    void testSave() {

        Card card = Card.builder()
                .id(1L)
                .cardNumber("12345")
                .password("1234")
                .build();

        CardDto build = CardDto.builder()
                .cardNumber("1234")
                .password("1234")
                .build();

        when(mapper.dtoToModel(build)).thenReturn(card);
        when(mapper.modelToDto(repository.save(card))).thenReturn(build);

        CardDto save = this.autorizadorService.save(build);
        Assertions.assertEquals("1234", save.getCardNumber());

    }

    @Test
    void getBalance() {

        Card card = Card.builder()
                .id(1L)
                .cardNumber("12345")
                .password("1234")
                .balance(BigDecimal.valueOf(500))
                .build();

        when(repository.findByCardNumber("123456")).thenReturn(Optional.of(card));
        BigDecimal balance = this.autorizadorService.getBalance("123456");

        Assertions.assertEquals(BigDecimal.valueOf(500), balance);

    }

    @Test
    void transaction() {

        CardTransactionDto transactionDto = CardTransactionDto.builder()
                .cardNumber("123456")
                .password("1234")
                .balance(BigDecimal.valueOf(500))
                .build();

        Card card = Card.builder()
                .id(1L)
                .cardNumber("123456")
                .password("1234")
                .balance(BigDecimal.valueOf(500))
                .build();

        CardDto build = CardDto.builder()
                .cardNumber("1234")
                .password("1234")
                .build();

        when(mapper.dtoToModel(build)).thenReturn(card);
        when(repository.findByCardNumber("123456")).thenReturn(Optional.of(card));
        when(repository.findByCardNumberAndPassword("123456", "1234")).thenReturn(Optional.of(card));
        when(repository.save(card)).thenReturn(card);
        when(repository.checkBalance("123456", BigDecimal.valueOf(500))).thenReturn(Optional.of(card));

        String transaction = this.autorizadorService.transaction(transactionDto);

        Assertions.assertEquals("Ok", transaction);
    }
}