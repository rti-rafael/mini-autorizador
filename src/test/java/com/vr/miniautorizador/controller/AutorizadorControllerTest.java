package com.vr.miniautorizador.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vr.miniautorizador.entity.dtos.CardDto;
import com.vr.miniautorizador.entity.dtos.CardTransactionDto;
import com.vr.miniautorizador.service.AutorizadorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = AutorizadorController.class)
class AutorizadorControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AutorizadorService autorizadorService;

    @Test
    void save() throws Exception {
        CardDto dto = CardDto.builder()
                .id(1L)
                .cardNumber("123456")
                .password("1234")
                .build();

        ResultActions response = mockMvc.perform(post("/autorizador/save")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJson(dto)));


        response.andExpect(status().isCreated());
    }

    @Test
    void getBalance() throws Exception {
        ResultActions response = mockMvc.perform(get("/autorizador/transaction")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));


        response.andExpect(status().isOk());
    }

    @Test
    void transaction() throws Exception {

        CardTransactionDto dto = CardTransactionDto.builder().build();

        ResultActions response = mockMvc.perform(post("/autorizador/transaction")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertToJson(dto)));


        response.andExpect(status().isCreated());
    }
    public  String convertToJson(Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }



}