package com.vr.miniautorizador.handlerException;

import com.vr.miniautorizador.entity.enums.StatusEnum;
import com.vr.miniautorizador.exception.ExistingCardException;
import com.vr.miniautorizador.exception.InsufficientFundsException;
import com.vr.miniautorizador.exception.InvalidPasswordException;
import com.vr.miniautorizador.exception.NotFoundCardException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class AutorizadorHandler {

    public static final String TIMESTAMP = "timestamp";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";

    @ExceptionHandler(ExistingCardException.class)
    public ResponseEntity<Map<String, Object>> handleExistingCardException(ExistingCardException msg) {

        Map<String, Object> error = new LinkedHashMap<>();

        error.put(TIMESTAMP, LocalDateTime.now());
        error.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY);
        error.put(MESSAGE, msg.getMessage());
        return ResponseEntity.unprocessableEntity().body(error);
    }


    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity< Map<String, Object>> handleInvalidPasswordException() {

        Map<String, Object> error = new LinkedHashMap<>();

        error.put(TIMESTAMP, LocalDateTime.now());
        error.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY);
        error.put(MESSAGE, StatusEnum.SENHA_INVALIDA);
        return ResponseEntity.unprocessableEntity().body(error);
    }

    @ExceptionHandler(InsufficientFundsException.class)
    public ResponseEntity< Map<String, Object>> handleInsufficientFundsException() {

        Map<String, Object> error = new LinkedHashMap<>();

        error.put(TIMESTAMP, LocalDateTime.now());
        error.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY);
        error.put(MESSAGE, StatusEnum.SALDO_INSUFICIENTE);
        return ResponseEntity.unprocessableEntity().body(error);
    }


    @ExceptionHandler(NotFoundCardException.class)
    public ResponseEntity<Void> handleNotFoundCardException() {
        return ResponseEntity.notFound().build();
    }
}
