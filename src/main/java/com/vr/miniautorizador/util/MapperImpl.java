package com.vr.miniautorizador.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vr.miniautorizador.entity.Card;
import com.vr.miniautorizador.entity.dtos.CardDto;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class MapperImpl implements Mapper {

    public Card dtoToModel(CardDto dto) {
        return Card.builder()
                .id(dto.getId())
                .cardNumber(dto.getCardNumber())
                .password(dto.getPassword())
                .build();
    }

    public  CardDto modelToDto(Card autorizador) {
        return CardDto.builder()
                .id(autorizador.getId())
                .cardNumber(autorizador.getCardNumber())
                .password(autorizador.getPassword())
                .build();
    }

    public  String convertToJson(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }
}
