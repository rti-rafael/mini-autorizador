package com.vr.miniautorizador.util;

import com.vr.miniautorizador.entity.Card;
import com.vr.miniautorizador.entity.dtos.CardDto;

public interface Mapper {

    Card dtoToModel(CardDto dto);

    CardDto modelToDto(Card autorizador);

    String convertToJson(Object obj);
}
