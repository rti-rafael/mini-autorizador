package com.vr.miniautorizador.service;

import com.vr.miniautorizador.entity.dtos.CardDto;
import com.vr.miniautorizador.entity.dtos.CardTransactionDto;
import com.vr.miniautorizador.exception.ExistingCardException;

import java.math.BigDecimal;

public interface AutorizadorService {

    CardDto save(CardDto autorizador) throws ExistingCardException;

    BigDecimal getBalance(String cardNumber);

    String transaction(CardTransactionDto cardTransaction);
}
