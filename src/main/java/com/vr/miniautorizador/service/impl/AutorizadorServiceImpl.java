package com.vr.miniautorizador.service.impl;

import com.vr.miniautorizador.entity.Card;
import com.vr.miniautorizador.entity.dtos.CardDto;
import com.vr.miniautorizador.entity.dtos.CardTransactionDto;
import com.vr.miniautorizador.exception.ExistingCardException;
import com.vr.miniautorizador.exception.InsufficientFundsException;
import com.vr.miniautorizador.exception.InvalidPasswordException;
import com.vr.miniautorizador.exception.NotFoundCardException;
import com.vr.miniautorizador.repository.AutorizadorRepository;
import com.vr.miniautorizador.service.AutorizadorService;
import com.vr.miniautorizador.util.Mapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Log4j2
@Transactional
@CacheConfig(cacheNames = "card")
public class AutorizadorServiceImpl implements AutorizadorService {

    private final AutorizadorRepository repository;

    private final Mapper mapper;

    public AutorizadorServiceImpl(AutorizadorRepository repository, Mapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public CardDto save(CardDto autorizador) throws ExistingCardException {

        try {
            Card card = mapper.dtoToModel(autorizador);
            log.info("Inserindo crédito de 500 reais.");
            card.setBalance(BigDecimal.valueOf(500));
            log.info("Efetuando cadastro...");
            return mapper.modelToDto(repository.save(card));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new ExistingCardException("Cartão já cadastrado!");
        }

    }

    @Override
    @CacheEvict(value = "card")
    public BigDecimal getBalance(String cardNumber) {
        return findByCardNumber(cardNumber).getBalance();
    }

    @Override
    @Cacheable
    public String transaction(CardTransactionDto cardTransaction) {
        findByCardNumber(cardTransaction.getCardNumber());
        verifyPassword(cardTransaction);
        Card card = repository.checkBalance(cardTransaction.getCardNumber(), cardTransaction.getBalance())
                .orElseThrow(InsufficientFundsException::new);
        card.setBalance(card.getBalance().subtract(cardTransaction.getBalance()));
        this.repository.save(card);

        return "Ok";
    }

    private Card findByCardNumber(String cardNumber) {
        log.info("Verificando se o cartão já existe.");
         return this.repository.findByCardNumber(cardNumber)
                .orElseThrow(NotFoundCardException::new);
    }

    private void verifyPassword(CardTransactionDto cardTransaction) {
        log.info("Verificando password.");
         this.repository.findByCardNumberAndPassword(cardTransaction.getCardNumber(), cardTransaction.getPassword())
                .orElseThrow(InvalidPasswordException::new);
    }


}
