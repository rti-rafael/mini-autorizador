package com.vr.miniautorizador.exception;

public class ExistingCardException extends RuntimeException {
    public ExistingCardException(String message) {
        super(message);
    }
}
