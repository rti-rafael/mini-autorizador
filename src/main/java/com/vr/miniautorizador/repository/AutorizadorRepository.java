package com.vr.miniautorizador.repository;

import com.vr.miniautorizador.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface AutorizadorRepository extends JpaRepository<Card, Long> {

    Optional<Card> findByCardNumber(String cardNumber);


    Optional<Card> findByCardNumberAndPassword(String cardNumber, String password);

    @Query(value = "select * from miniautorizador.card c \n" +
            "where c.card_number = :cardNumber AND c.balance >= :balance", nativeQuery = true)
    Optional<Card> checkBalance(String cardNumber, BigDecimal balance);
}
