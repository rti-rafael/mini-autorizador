package com.vr.miniautorizador.entity.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CardDto {

    private Long id;

    @NonNull
    @JsonProperty("numeroCartao")
    private String cardNumber;

    @NonNull
    @JsonProperty("senha")
    private String password;
}
