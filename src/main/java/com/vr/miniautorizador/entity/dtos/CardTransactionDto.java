package com.vr.miniautorizador.entity.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CardTransactionDto {

    @JsonProperty("numeroCartao")
    private String cardNumber;

    @JsonProperty("senhaCartao")
    private String password;

    @JsonProperty("valor")
    private BigDecimal balance;
}
