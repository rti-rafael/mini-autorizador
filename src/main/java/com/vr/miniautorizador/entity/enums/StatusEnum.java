package com.vr.miniautorizador.entity.enums;

public enum StatusEnum {

    SALDO_INSUFICIENTE, SENHA_INVALIDA, CARTAO_INEXISTENTE
}
