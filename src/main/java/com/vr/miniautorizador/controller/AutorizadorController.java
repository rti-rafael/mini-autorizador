package com.vr.miniautorizador.controller;

import com.vr.miniautorizador.entity.dtos.CardDto;
import com.vr.miniautorizador.entity.dtos.CardTransactionDto;
import com.vr.miniautorizador.exception.ExistingCardException;
import com.vr.miniautorizador.service.AutorizadorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/autorizador")
@AllArgsConstructor
public class AutorizadorController {

    private final AutorizadorService autorizadorService;

    @PostMapping("/save")
    public ResponseEntity<CardDto> save(@RequestBody @Validated CardDto card) throws ExistingCardException {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.autorizadorService.save(card));

    }

    @GetMapping("/{numeroCartao}")
    public ResponseEntity<BigDecimal> getBalance(@PathVariable("numeroCartao") String cardNumber) {

        return ResponseEntity.ok(this.autorizadorService.getBalance(cardNumber));

    }

    @PostMapping("/transaction")
    public ResponseEntity<String> transaction(@RequestBody @Validated CardTransactionDto transaction) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.autorizadorService.transaction(transaction));

    }
}
