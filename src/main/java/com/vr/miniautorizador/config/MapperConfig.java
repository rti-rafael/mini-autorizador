package com.vr.miniautorizador.config;

import com.vr.miniautorizador.util.Mapper;
import com.vr.miniautorizador.util.MapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean
    public Mapper createBeanMapper(){
        return new MapperImpl();
    }
}
