# Mini Autorizador API
Este projeto é destinado a transações com cartão da VR.


## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [JDK 11: Necessário para executar o projeto Java](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
- [Maven 3++: Necessário para realizar o build do projeto Java](https://maven.apache.org/download.cgi)
- [Docker: Necessário para rodar o compose](https://www.docker.com/)
- [Git: Necessário para clonar o projeto](https://git-scm.com/)


## Desenvolvimento

Para iniciar o desenvolvimento, é necessário clonar o projeto [mini-autorizador](https://gitlab.com/rti-rafael/mini-autorizador.git) do GitLab em um diretório de sua preferência:

- `mini-autorizador`: git clone https://gitlab.com/rti-rafael/mini-autorizador.git


## Construção (Build)

Para construir o projeto com o Maven, executar os comando abaixo:

- `mvn clean install` executar esse comando para baixar as dependências
- `mvn clean install -P dev` para rodar em ambiente DEV

O comando irá baixar todas as dependências do projeto e criar um diretório target com os artefatos construídos, que incluem o arquivo jar do projeto. Além disso, serão executados os testes unitários, e se algum falhar, o Maven exibirá essa informação no console.


## Commits

Faça comite nas suas branch de desenvolvimento, faça um `merge-request` e solicite ao seu DEV-LEAD ou um dev com mais experiência para (rever) o seu código e aprovar o merge.


## Tecnologias utilizadas

- [Spring-boot](https://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html)
- [Maven](https://maven.apache.org/index.html)
- [Java 11](https://www.oracle.com/java/technologies/javase-jdk8-doc-downloads.html)
- [Lombok](https://projectlombok.org/)
- [Mysql](https://www.mysql.com/downloads/)
- [Spring cache](https://spring.io/guides/gs/caching/)
- [Docker](https://docs.docker.com/desktop/install/)

## DB's

A aplicação necessita de bancos de dados relacionais (MySQL, ou H2 em caso de uma execução local/desenvolvimento):

- [MySQL](https://www.mysql.com/downloads/)

## Boas práticas

- Faça testes unitários de todas as suas chamadas e regras de negócio, tente manter uma cobertura minimia de 80%

- Sempre se certifique de não ter quebrado nenhum outro código.

- Faça build do projeto e execute-o antes de fazer commit das suas alterações.


## Variáveis do projeto

 Diretório     | Local
 -----------|--------------------------
 dev     | -Dspring.profiles.active=dev






## Rodando os testes

Para rodar os testes, realize o seguinte comando

```bash
# Rodar todos os testes 
$ mvn test

# Rodar penas uma classe
$ mvn -Dtest=TestApp1 test
```


## Rodando local

Clonar o projeto no diretorio de sua preferencia

```bash
  git clone https://gitlab.com/rti-rafael/mini-autorizador.git
```

Ir para o diretorio do projeto

```bash
  cd mini-autorizador
```

Instalar dependencias

```bash
  mvn clean install
```

Setar o profile conforme ambiente

```bash
  -Dspring.profiles.active=dev
```

Subir o docker compose para o Mysql

```bash
  docker-compose up
```

Rodar a aplicação

```bash
  mvn spring-boot:run
```


